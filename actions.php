<?php
if (!function_exists('add_action')) {
    return;
}

/**
 * Kindling Config - Actions
 *
 * @package Kindling_Config
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

add_action('kindling_ready', function () {
    // Load environment variables
    try {
        $dotenv = Dotenv\Dotenv::createImmutable(ABSPATH);
        $dotenv->load();
    } catch (\Exception $e) {
    }
});

<?php
/**
 * Kindling Config - Helpers
 *
 * @package Kindling_Config
 * @author  Matchbox Design Group <info@matchboxdesigngroup.com>
 */

function config($key = null)
{
    $directories = apply_filters('kindling_config_directories', [
        __DIR__ . "/config",
        get_template_directory() . "/config",
        get_stylesheet_directory() . "/config",
    ]);
    $config = (new \Kindling\Config\Config(
        $directories
    ))->getSingleton();

    return $key ? $config->get($key) : $config;
}

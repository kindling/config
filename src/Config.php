<?php

namespace Kindling\Config;

use Illuminate\Config\Repository;
use Illuminate\Contracts\Container\Container as ContainerContract;

class Config
{
    protected $directories;
    protected $abstract;

    public function __construct(array $directories = [], string $abstract = 'kindling-config.repository')
    {
        $this->directories = collect($directories)->unique();
        $this->abstract = $abstract;
    }

    public function getSingleton()
    {
        try {
            return kindling($this->abstract);
        } catch (\Exception $e) {
            $this->setup();
            return kindling($this->abstract);
        }
    }

    protected function setup()
    {
        kindling()->singleton($this->abstract, function (ContainerContract $app) {
            return new Repository($this->items());
        });
    }

    protected function items()
    {
        return $this->directories
        ->map(function ($directory) {
            return $this->configs($directory);
        })
        ->flatten()
        ->filter()
        ->mapWithKeys(function ($config) {
            return [pathinfo($config, PATHINFO_FILENAME) => require $config];
        })
        ->toArray();
    }

    protected function configs($directory)
    {
        $configs = glob(rtrim($directory, '/') . '/*.php');

        return collect($configs ? $configs : []);
    }
}
